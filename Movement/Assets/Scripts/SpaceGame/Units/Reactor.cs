﻿using UnityEngine;
using System.Collections;

public class Reactor : CoreScript {
	
	public tier tier1, tier2, tier3;
	public int TOTAL_POWER;//power currently stored

	public int newValue;

	public int units;


	public void Update()
	{
		if (Input.GetKeyDown (KeyCode.Space)) 
		{
			inputUnits (newValue);
			for (int i = 0; i < 3; i++) 
			{
				print(tierChecker(tier1,units,i));
				break;
			}
		} 
		else if (Input.GetKeyDown (KeyCode.KeypadEnter)) 
		{
			outPutUnits (newValue);
			for (int i = 0; i < 3; i++) 
			{
				print(tierChecker(tier1,units,i));
				break;
			}
		}
		//TOTAL_POWER = powerTypes(upgradeReactor (0),units);
	}


	public void inputUnits(int currentValue)
	{
		units += currentValue;
	}

	public void outPutUnits(int currentValue)
	{
		if (currentValue <= units) //this makes it so you cannot remove units larger than the currently stored units, meaning it won't become negative
		{ 
			units -= currentValue;
		} 
		else 
		{
			Debug.LogWarning ("Units cannot be lower than ZERO (0)");
		}
	}

	/*
	private int outPutReactor(int units, tier currentTier)//reactor room has all the properties of the reactor
	{
		return powerTypes (currentTier, units);
	}
	*/


	private tier upgradeReactor(int current)
	{
		switch (current) 
		{
			case 0:
				return tier1;
			case 1:
				return tier2;
			case 2:
				return tier3;
		}
		return new tier();
	}
}
