﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public struct tier
{
	public int[] unitsNeeded;
	public int min,mid,max;
}
public abstract class CoreScript : MonoBehaviour {

	//public int global_UNIT;
	//public int PossibleUnits; //delegation of units is requird for systems to do something
	
	//public int Battery; //reactor will add values into battery, how ever, it cannot go past a determined value;
	
	public int tierChecker(tier checkStruct, int units,int i)
	{
		if (units >= checkStruct.unitsNeeded [i])
		{
			return outputEnhance (checkStruct, i);
		}
		return 0;
	}

	private int outputEnhance(tier checkStruct,int value)
	{
		switch (value) 
		{
			case 0:
				return checkStruct.min;
			case 1:
				return checkStruct.mid;
			case 2:
				return checkStruct.max;
		}
		return 0;
	}
		
	public void delegation(int value)//this function will be called every time the player wants to place units some where
	{

	}

	public void removeDelegates(int value) //this function will be called every time it needs to remove delegates
	{

	}

	public void Engine()
	{

	}

}