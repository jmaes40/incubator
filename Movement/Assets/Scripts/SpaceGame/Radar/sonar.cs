﻿using UnityEngine;
using System.Collections;

public class sonar :  MonoBehaviour{

	private RaycastHit returnedRay;
	public LayerMask layer;
	public bool toggle;
	public int value;
	public Material newMaterial;
	public GameObject prefab;
	public checkingCollisions radarCollisionChecker;

	public void LateUpdate()
	{
		if(toggle)
		{
			cycle(value);
			toggle =! toggle;
		}
	}

	private void cycle(int value)
	{
		radarCollisionChecker.toggle();
		//print (radarCollisionChecker.toggle);
		prefab.transform.localScale = new Vector3 (value,value, value);
		radarCollisionChecker.toggle();
		//showObject (radarCollisionChecker.storedTransforms);
	}


	public void showObject(Transform[] objects)
	{
		if (objects.Length > 0) 
		{
			for (int i = 0; i < objects.Length; i++) 
			{
				objects [i].gameObject.GetComponent<Renderer> ().material.color = Color.red;
				//print ("working");
			}

		}
		print (objects.Length);
	}

	private bool castSphere(Vector3 localPosition, float radius)
	{
		return Physics.SphereCast(localPosition,radius,localPosition,out returnedRay,(float)radius,layer);
		//return Physics.SphereCast(,,,,,,
	}

	//private Vector3 value()
}
