﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class checkingCollisions : MonoBehaviour {

	public LayerMask layer;
	private List<Transform> foundTransforms = new List<Transform>();
	public Transform[] storedTransforms;
	public sonar sonarSystem;
	public displayInUi spawnUI;
	private bool canCycle;

	private void OnTriggerStay(Collider col)
	{
		if (col.gameObject)
		{
			if (toggle()) 
			{
				foundTransforms.Add (col.gameObject.transform);
			}
		}
	}

	private void Update()
	{
		storeTransforms ();
	}

	private void storeTransforms()
	{
		if (foundTransforms.Count > 1 && !toggle()) 
		{
			storedTransforms = new Transform[foundTransforms.Count];
			for (int i = 0;i < foundTransforms.Count; i++) 
			{
				storedTransforms [i] = foundTransforms [i];
			}
			foundTransforms = new List<Transform> ();
			sonarSystem.showObject (storedTransforms);
			spawnUI.applyToUi (storedTransforms);
			//Destroy (gameObject);
		}
		//Destroy (gameObject);
	}

	public bool toggle()
	{
		return canCycle = !canCycle;
	}
}
