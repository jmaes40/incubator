﻿using UnityEngine;
using System.Collections;

public class displayInUi : MonoBehaviour {

	public GameObject prefab;
	private GameObject tempObject;
	public checkingCollisions destroyObject;

    public void applyToUi(Transform[] storedPositions)
    {
		if (storedPositions.Length > 1) 
		{
			foreach (Transform i in storedPositions)
			{
				tempObject = Instantiate (prefab, i.position/100 + transform.position, Quaternion.identity) as GameObject;
				tempObject.transform.parent = transform;
			}
			Destroy (destroyObject.gameObject);
			tempObject = null;
		}
	}
}
