﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class stages : MonoBehaviour {

	public float timer, totalTime;
	private int timerInt;
	public Text textBox;
	private delegate void Switcher();
	private event Switcher eventSwitcher;
	public MoveObeject buildingPhaseObject;
	public GameObject Player;
	private int i = 0;
	private void Start()
	{
		eventSwitcher = MainStage;
		StartCoroutine(calcTime());
	}

	private void Update()
	{
		timerInt = (int)timer;

		textBox.text = timerInt.ToString();
		eventSwitcher();
	}

	private IEnumerator calcTime()
	{
		while(timer > 0)
		{
			timer -= Time.deltaTime;
			yield return null;
		}
		cycleFunctions(i += 1);
	}

	private void cycleFunctions(int value)
	{
		print(value);
		switch(value)
		{
			case 1:
			StopAllCoroutines();
			timer = 30f;
			eventSwitcher = null;
			eventSwitcher += MiddleStage;
			StartCoroutine(calcTime());
				break;
			case 2:
			StopAllCoroutines();
			eventSwitcher = null;
			eventSwitcher = EndStage;
				break;
		}
	}

	//there are 3 main stages, construction stage, escape stage and win stage
	private void MainStage()
	{
		buildingPhaseObject.ActionsOfObject();
	}

	private void MiddleStage()//escape stage
	{
		
	}

	private void EndStage()//end game stage
	{
		
	}



}
