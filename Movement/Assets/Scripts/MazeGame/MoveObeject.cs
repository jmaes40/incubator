﻿using UnityEngine;
using System.Collections;

public class MoveObeject : MonoBehaviour {

	public rules runScript;
	public bool canCheck;
	private float speed = 8.0f;
	private Vector3 pos;
	private Transform tr;
	public string[] nameArray;
	public GameObject[] Tools;
	private void Start() 
	{
		//swapObject(Random.Range(0,nameArry.Length));
		swapObject(0);
		runScript.cycleArray(gameObject);
		pos = transform.position;
		tr = transform;
	}

	public void ActionsOfObject()
	{
		if (Input.GetKey(KeyCode.RightArrow) && tr.position == pos)
		{
			canCheck = true;
			runScript.emptyArray();
			pos += Vector3.right;
		}
		else if (Input.GetKey(KeyCode.LeftArrow) && tr.position == pos)
		{
			canCheck = true;
			runScript.emptyArray();
			pos += Vector3.left;
		}
		else if (Input.GetKey(KeyCode.UpArrow) && tr.position == pos)
		{
			canCheck = true;
			runScript.emptyArray();
			pos += Vector3.forward;
		}
		else if (Input.GetKey(KeyCode.DownArrow) && tr.position == pos)
		{
			canCheck = true;
			runScript.emptyArray();
			pos += Vector3.back;
		}

		transform.position = Vector3.MoveTowards(transform.position, pos, Time.deltaTime * speed);
		if(tr.position == pos && canCheck)
		{
			runScript.cycleArray(gameObject);
			canCheck = !canCheck;
		}

		if(Input.GetKeyDown(KeyCode.Space))
		{
			if(runScript.storedGrid.Count > 1)
			{
				foreach (GameObject i in Tools)
				{
					if(i.activeSelf == true)
					{
						runScript.placeObject(i,gameObject, i.transform.rotation, runScript.storedGrid[1/runScript.storedGrid.Count].transform.position);
					}
				}
			}
			else if(runScript.storedGrid.Count == 1)
			{
				foreach (GameObject i in Tools)
				{
					if(i.activeSelf == true)
					{
						runScript.placeObject(i,gameObject, i.transform.rotation, runScript.storedGrid[0].transform.position);
					}
				}
			}
		}
		if(Input.GetKeyDown(KeyCode.E))
		{
			rotate(90f);
		}
		else if(Input.GetKeyDown(KeyCode.Q))
		{
			rotate(-90f);
		}
		checkKeys();
	}  

	private void rotate(float direction)
	{
		runScript.emptyArray();
		transform.Rotate(0f,direction,0f);
		runScript.cycleArray(gameObject);
	}

	private void checkKeys()
	{
		if(Input.GetKeyDown(KeyCode.Alpha1))
		{
			swapObject(0);
		}
		else if(Input.GetKeyDown(KeyCode.Alpha2))
		{
			swapObject(1);			
		}
		else if(Input.GetKeyDown(KeyCode.Alpha3))
		{
			swapObject(2);
		}
	}

	private void swapObject(int swap)
	{
		runScript.emptyArray();
		gameObject.name = nameArray[swap];
		foreach (GameObject i in Tools)
		{
			if(i.activeSelf == true)
			{
				i.SetActive(false);
			}
		}
		Tools[swap].SetActive(true);
		runScript.cycleArray(gameObject);
	}
}
