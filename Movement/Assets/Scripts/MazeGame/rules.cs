﻿//DrayRaw(prefab,directionRay(prefab.transform,i));

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class rules : objectClass {

	public Material savedMaterial, originalColour, rejectMaterial;

	public void cycleArray(GameObject prefab)
	{
		foreach(int i in checkArray(selectedType(prefab)))
		{
			if(castRay(prefab,directionRay(prefab.transform,i),currentLayer))
			{
				checkCast(returnedRay.collider.gameObject,i, prefab);
			}

		}
		colourGrid();
	}

	public void checkCast(GameObject checkedGrid, int i, GameObject prefab)
	{
		if(selectedType(prefab) != CLASS_TYPE.HAZZARD)
		{
			if(directionRay(checkedGrid.transform,i) == Vector3.zero)
			{
				if(!Physics.Raycast(checkedGrid.transform.position,transform.up,1f,newLayerMask))
				{
					rejectedGrid.Add(checkedGrid);
				}								
				else
				{
					storedGrid.Add(checkedGrid);		
				}
			}
			else
			{
				if(!Physics.Raycast(checkedGrid.transform.position,transform.up,1f,otherLayer))
				{
					storedGrid.Add(checkedGrid);//if there is nothing obove the grid space, it will add to the list		
				}								//else it will ignore adding it
				else
				{
					rejectedGrid.Add(checkedGrid);
				}
			}
		}
		else
		{
			if(!Physics.Raycast(checkedGrid.transform.position,transform.up,1f,otherLayer))
			{
				storedGrid.Add(checkedGrid);//if there is nothing obove the grid space, it will add to the list		
			}								//else it will ignore adding it
			else
			{
				rejectedGrid.Add(checkedGrid);
			}
		}

	}

	private void colourGrid()
	{
		if(storedGrid.Count > 0)
		{
			foreach(GameObject i in storedGrid)//colours allowed spaces
			{
				i.GetComponent<Renderer>().material = savedMaterial;
			}
		}
		if(rejectedGrid.Count > 0)
		{
			foreach(GameObject i in rejectedGrid)//colours not allowed spaces
			{
				i.GetComponent<Renderer>().material = rejectMaterial;
			}	
		}
	}

	public void emptyArray()
	{
		if(storedGrid.Count > 0)
		{
			foreach(GameObject i in storedGrid)
			{
				i.GetComponent<Renderer>().material = originalColour;
			}	
			storedGrid = new List<GameObject>();
		}
		if(rejectedGrid.Count > 0)
		{
			foreach(GameObject i in rejectedGrid)
			{
				i.GetComponent<Renderer>().material = originalColour;
			}	
			rejectedGrid = new List<GameObject>();
		}
	}

	public void placeObject(GameObject prefab, GameObject managerName, Quaternion rotation, Vector3 pos)
	{
		if(storedGrid.Count > 0 && rejectedGrid.Count == 0)
		{
			GameObject newObject = Instantiate(prefab,new Vector3(pos.x, pos.y + .1f, pos.z) ,rotation) as GameObject;
			if(selectedType(managerName) != CLASS_TYPE.HAZZARD)
			{
				print ("i am not a hazzard");
				newObject.layer = 8;
			}
			else
			{
				newObject.layer = 10;
			}
			Destroy(newObject.GetComponent<MoveObeject>());

			newObject = null;
			emptyArray();
			cycleArray(prefab);
		}
	}
}