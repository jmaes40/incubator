﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum CLASS_TYPE
{
	BRIDGE,
	HAZZARD,
	SPECIAL_BRIGE,
	DELETE_TOOL,
	NOTHING
};
public class objectClass : MonoBehaviour {

	public CLASS_TYPE classes;
	public List<GameObject> storedGrid, rejectedGrid;
	public LayerMask currentLayer, otherLayer, newLayerMask;		

	public RaycastHit returnedRay;
	//public GameObject prefab;
	private int[] checkSpaces;
	public float maxDistance;

	//this class will contain the possibleObjects Needed to check Cube Spaces
	//i have to figure out how spaces are used per object

	public int[] checkArray(CLASS_TYPE checkClass)
	{
		switch(checkClass)
		{
			case CLASS_TYPE.BRIDGE:
			checkSpaces = new int[3]{1,0,2};
			break;

			case CLASS_TYPE.HAZZARD:
			checkSpaces = new int[1]{1};
			break;

			case CLASS_TYPE.SPECIAL_BRIGE:
			checkSpaces = new int[3]{1,2,3};
			break;

			case CLASS_TYPE.DELETE_TOOL:
			checkSpaces = new int[3]{0,1,2};
			break;
		}
		return checkSpaces;
	}
		
	public CLASS_TYPE selectedType(GameObject usedObject)
	{
		switch(usedObject.name)
		{
			case "Bridge":
				return CLASS_TYPE.BRIDGE;

			case "Hazzard":
				return CLASS_TYPE.HAZZARD;

			case "Special_Bridge":
				return CLASS_TYPE.SPECIAL_BRIGE;

			case "DELETE_TOOL":
				return CLASS_TYPE.DELETE_TOOL;
		}
		return CLASS_TYPE.NOTHING;
	}

	public bool castRay(GameObject storedObject, Vector3 direction, LayerMask newLayer)
	{
		return Physics.Raycast(storedObject.transform.position + direction, -transform.up, out returnedRay, maxDistance, newLayer);
	}

	public void DrayRaw(GameObject storedObject, Vector3 direction)
	{
		Debug.DrawRay(storedObject.transform.position + direction, -transform.up * maxDistance ,Color.red,100f);
	}
		
	public Vector3 directionRay(Transform savedObject,int interger)
	{
		Vector3 newPos = new Vector3();
		switch(interger)
		{
		case 0:
			newPos = -savedObject.transform.right;
			break;
		case 1:
			newPos = Vector3.zero;
			break;
		case 2:
			newPos = savedObject.transform.right;
			break;
		case 3: 
			newPos = -savedObject.transform.forward;
			break;
		}
		return newPos;
	}
}