﻿using UnityEngine;
using System.Collections;
[ExecuteInEditMode]
public class Grid : MonoBehaviour {

	public float Xpos, Zpos, offset;
	public bool toggle;
	public GameObject prefab;
	private void Update()
	{
		if(toggle)
		{
			cycle();
			toggle =! toggle;
		}
	}


	private void cycle()
	{
		for(int x = 0; x < Xpos; x++)
		{
			for(int z = 0; z < Zpos; z++)
			{
				Instantiate(prefab,new Vector3(Random.Range(0,100f),Random.Range(0,100f),Random.Range(0,100f)/*x * offset,0f,z * offset*/),Quaternion.identity);
			}
		}
	}
		
}
