﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {

	public Transform target;
	public float offset, speed, jumpHeight;
	private Rigidbody rb;
	public bool canJump;
	public float savedMomentum;
	//public Vector3 visualize;

	private void Start()
	{
		rb = GetComponent<Rigidbody>();	
		canJump = true;
	}

	private void Update()
	{
		if (Input.GetKey (KeyCode.W) || Input.GetKey (KeyCode.S)) 
		{
			if(direction("Vertical") > 0 && canJump == true || direction("Vertical") < 0 && canJump == true)
			{
				transform.Translate(transform.forward * direction("Vertical") * speed);
				transform.LookAt(target.position);
			}
		}
		if (Input.GetKey (KeyCode.A) || Input.GetKey (KeyCode.D)) 
		{
			if ((direction ("Horizontal") > 0 && canJump == true || direction ("Horizontal") < 0) && canJump == true) 
			{
				transform.Translate (-transform.right * direction ("Horizontal") * speed);
				transform.LookAt (target.position);
			}
		}

		if(canJump)
		{
			//savedMomentum = rb.velocity.magnitude;
		}

		target.transform.position = new Vector3(transform.position.x + direction("Horizontal") * offset
			, transform.position.y, transform.position.z + direction("Vertical") * offset);
	}
	/*
	private void FixedUpdate()
	{
	//	visualize = new Vector3(rb.velocity.x,0f, rb.velocity.z);
		if(Input.GetKeyDown(KeyCode.Space) && canJump)
		{
			//jump();
			//canJump = false;

		}
	}
	*/

	private void jump()
	{
		rb.AddForce(transform.up * jumpHeight, ForceMode.Impulse);
		if(rb.velocity.magnitude > 0)
		{
			rb.AddForce(transform.forward * jumpHeight, ForceMode.Impulse);
		}
	}
				
	public float direction(string movementName)
	{
		return Input.GetAxis(movementName);
	}

	/*
	private void OnCollisionStay(Collision col)
	{
		if(col.gameObject.tag == "floor")
		{
			foreach(ContactPoint contact in col.contacts)
			{
				print("Running");
				if(contact.point.y < transform.position.y)
				{
					canJump = true;
				}
				else
				{
					print(contact.point);
				}

			}
		}
	}

	private void OnCollisionExit(Collision col)
	{
	 	if(col.gameObject.tag == "floor")
		{
				
		}
	}
	*/
}
